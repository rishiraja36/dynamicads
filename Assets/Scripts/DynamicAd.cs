﻿using System;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class DynamicAd : MonoBehaviour
{
    public Layer FrameLayer,TextLayer;
    public string textUrl, frameUrl;

    public Image image;
    public RectTransform textRect;
    public Text adText;
    DownloadHandler downloadHandler;
    public TextAsset tAsset;

    void Awake()
    {
        downloadHandler = GetComponent<DownloadHandler>();
    }

    void OnEnable()
    {
        downloadHandler.OnDownload += OnDownloaded;
        downloadHandler.OnImageDownload += OnImageDownloaded;
    }

    private void OnImageDownloaded(Sprite obj)
    {
        if(obj==null)
        {
            adText.text = Utility.invalid;
            Debug.Log("Invalid Ad!");
        }
        else
        {
            image.sprite = obj;
        }
    }

    void OnDownloaded(string obj)
    {
        if(obj.Contains("Error"))
        {
            
            adText.text = Utility.invalid;
            Debug.Log("Invalid Ad!");
        }
        else
        {
            try
            {
                Ad deserializedClass = JsonMapper.ToObject<Ad>(obj);

                if (deserializedClass.layers[0].type == "frame")
                {
                    FrameLayer = deserializedClass.layers[0];
                    Debug.Log(FrameLayer.type);
                    downloadHandler.GetImageJson(deserializedClass.layers[0].path);

                    ApplyFrameConfig(FrameLayer.placement[0].position.x, FrameLayer.placement[0].position.y, FrameLayer.placement[0].position.width, FrameLayer.placement[0].position.height);

                }
                else
                {
                    TextLayer = deserializedClass.layers[0];
                    Debug.Log(TextLayer.type);

                    ApplyTextConfig("Order Now!", TextLayer.placement[0].position.x, TextLayer.placement[0].position.x, TextLayer.placement[0].position.width, TextLayer.placement[0].position.height);
                }

            }
            catch
            {
                adText.text = Utility.invalid;
                Debug.Log("Invalid Ad!");
            }

            
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        downloadHandler.GetJson(textUrl);
        downloadHandler.GetJson(frameUrl);
    }

    public void ApplyFrameConfig(int x, int y, int width, int height)
    {
    
        image.rectTransform.anchoredPosition = new Vector2(x, y);
        image.rectTransform.sizeDelta = new Vector2(width, height);

        if (FrameLayer.operations == null)
            return;

        if (!string.IsNullOrEmpty(FrameLayer.operations[0].argument))
        {
            Color color = new Color();
            ColorUtility.TryParseHtmlString(FrameLayer.operations[0].argument, out color);
            color.a = 1;
            image.color = color;
        }

    }

    public void ApplyTextConfig(string str, int x, int y, int width, int height)
    {
        adText.text = str;
        textRect.sizeDelta = new Vector2(width, height);
        textRect.anchoredPosition = new Vector2(x, y);

        if (TextLayer.operations == null)
            return;

        if (!string.IsNullOrEmpty(TextLayer.operations[0].argument))
        {
            Color color = new Color();
            ColorUtility.TryParseHtmlString(TextLayer.operations[0].argument, out color);
            color.a = 1;
            adText.color = color;
        }
    }
   
    
}
