﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using LitJson;
using UnityEngine;
using UnityEngine.Networking;

public static class Utility 
{
    public static string invalid = "Invalid Ad!";
    public static string order = "Order Now!";

    public static IEnumerator GetRequest(string url, Action<UnityWebRequest> callback)
    {
        using (UnityWebRequest request = UnityWebRequest.Get(url))
        {
            // Send the request and wait for a response
            yield return request.SendWebRequest();
            callback(request);
        }
    }

    public static IEnumerator ImageRequest(string url, Action<UnityWebRequest> callback)
    {
        using (UnityWebRequest req = UnityWebRequestTexture.GetTexture(url))
        {
            yield return req.SendWebRequest();
            callback(req);
        }
    }

}
