﻿
using System.Collections.Generic;

public class Position
{
    public int x { get; set; }
    public int y { get; set; }
    public int width { get; set; }
    public int height { get; set; }
}

public class Placement
{
    public Position position { get; set; }
}

public class Operation
{
    public string name { get; set; }
    public string argument { get; set; }
}

public class Layer
{
    public string type { get; set; }
    public string path { get; set; }
    public List<Placement> placement { get; set; }
    public List<Operation> operations { get; set; }
}

public class Ad
{
    public List<Layer> layers { get; set; }
}
