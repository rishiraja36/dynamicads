﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class DownloadHandler : MonoBehaviour,IDownload
{
    public event Action<string> OnDownload;
    public event Action<Sprite> OnImageDownload;

    public void GetImageJson(string s)
    {
        StartCoroutine(Utility.ImageRequest(s, (UnityWebRequest req) =>
        {
            if (req.isNetworkError || req.isHttpError)
            {
                Debug.Log($"{req.error}: {req.downloadHandler.text}");
            }
            else
            {
                // Get the texture out using a helper downloadhandler
                Texture2D texture = DownloadHandlerTexture.GetContent(req);
                // Save it into the Image UI's sprite
                if (texture != null)
                {
                    Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
                    OnImageDownload(sprite);
                }
                else
                {
                    OnImageDownload(null);
                }

            }
        }));
    }

    public void GetJson(string s)
    {
        StartCoroutine(Utility.GetRequest(s, (UnityWebRequest req) =>
        {
            if (req.isNetworkError || req.isHttpError)
            {
                Debug.Log($"{req.error}: {req.downloadHandler.text}");
                OnDownload("Error");
            }
            else
            {
                OnDownload(req.downloadHandler.text);
            }
        }));
    }

    
}

interface IDownload
{
    event Action<string> OnDownload;
    event Action<Sprite> OnImageDownload;
    void GetJson(string s);
    void GetImageJson(string s);
}
